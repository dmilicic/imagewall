import os

from django.http import HttpResponse, Http404
from django.utils import simplejson
from django import shortcuts
from slikozid.models import Slika, Hashtag
from django.shortcuts import render_to_response
from server.settings import MEDIA_ROOT, TAG_RADIUS, LAST_X_HOURS
from django.db.models import Q, Count, Max
from slikozid.utility import distance
from datetime import datetime, timedelta
import hashlib 
import imghdr

from django.views.decorators.csrf import csrf_exempt


# Create your views here.

#Izlistavanje posljednje aktivnih tagova na pocetnoj stranici
def index(request):
	
	nove_slike = Slika.objects.filter(updated_at__range = [ datetime.now() - timedelta(LAST_X_HOURS / 24), datetime.now() ])
	hashtags = Hashtag.objects.annotate(Count('slika')).order_by('slika__count').reverse()
	
	all_active_tags = set()
	for s in nove_slike:
		all_active_tags.add(s.HashtagID)
	
	tag_list = [ v for v in hashtags if v in all_active_tags ]
	
	return shortcuts.render(request, 'slikozid/index.html', {'tag_list': tag_list})

#Izlistavanje svih slika koje su pridruzene odredjenom tagu
def hash(request, hashtag):
	
	try:
		getTag = Hashtag.objects.get(slug = hashtag)
	except Hashtag.DoesNotExist:
		raise Http404
		
	img_list = Slika.objects.filter(HashtagID = getTag.pk).order_by('-pk')
	for image in img_list:
		wanted_width = 300
		image.height = image.height * wanted_width / image.width
		image.width = wanted_width
	
	if request.is_ajax():
		return shortcuts.render(request, 'render_images.html', {'img_list': img_list, 'hashtag': getTag})
	else:
		return shortcuts.render(request, 'slikozid/gallery.html', {'img_list': img_list, 'hashtag': getTag})

# Prikazivanje galerije na vanjskoj stranici - 'embedanje'
def embed(request, hashtag):
	
	try:
		getTag = Hashtag.objects.get(slug = hashtag)
	except Hashtag.DoesNotExist:
		raise Http404
		
	img_list = Slika.objects.filter(HashtagID = getTag.pk).order_by('-pk')
	for image in img_list:
		wanted_width = 300
		image.height = image.height * wanted_width / image.width
		image.width = wanted_width
	
	return shortcuts.render(request, 'slikozid/embed.html', {'img_list': img_list, 'hashtag': getTag})

def image(request, image_id):
	
	try:
		image = Slika.objects.get(pk = image_id)
	except Slika.DoesNotExist:
		raise Http404
		
	return shortcuts.render(request, 'slikozid/image.html', {'image': image})


def navigation_autocomplete(request,
    template_name='slikozid/autocomplete.html'):

    q = request.GET.get('q', '')
    context = {'q': q}

    queries = {}
    queries['hashtags'] = Hashtag.objects.filter(
        Q(tag__icontains=q)
    ).distinct()[:3]

    context.update(queries)

    return shortcuts.render(request, template_name, context)
	
#Slanje svih tagova u radijusu od TAG_RADIUS metara od lokacije uredjaja
@csrf_exempt
def tag_request(request):
	
	if request.method == 'POST':
	
		latitude = request.POST['latitude']
		longitude = request.POST['longitude']
		tag_data = dict()
		
		for slika in Slika.objects.all():
			if slika.longitude == None or slika.latitude == None: 
				continue
			dist = distance( float(latitude), float(longitude), float(slika.latitude), float(slika.longitude) )
			if dist <= TAG_RADIUS:
				value = tag_data.get(slika.HashtagID.tag, -1)
				if value == -1:
					tag_data[slika.HashtagID.tag] = dist
				else:
					tag_data[slika.HashtagID.tag] = min(tag_data[slika.HashtagID.tag], dist)
		
		tag_list = list()
		for tags in tag_data.keys():
			tag_list.append( (tags, tag_data[tags]) )
		
		tag_list.sort(key=lambda tup: tup[1])
		
		to_json = { "tags": [ tag[0] for tag in tag_list ][:20] }
		
		return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")
	raise Http404
	
#Dodavanje slike odredjenom hashtagu
@csrf_exempt
def update(request):
	
	if request.method == 'POST':
			
		tagName = request.POST['tagName']
		latitude = request.POST['latitude']
		longitude = request.POST['longitude']
		slika_id = request.POST['id']
		
		try:
			slika = Slika.objects.get(pk = slika_id)
		except Slika.DoesNotExist:
			to_json = {
				"success": 0,
			}
			return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")
	
		try:
			hashtag = Hashtag.objects.get(tag = tagName)
		except Hashtag.DoesNotExist:
			hashtag = Hashtag(tag = tagName)
			hashtag.save()
		
		slika.latitude = latitude
		slika.longitude = longitude
		slika.HashtagID = hashtag
		slika.save()
	
		to_json = {
			"success": 1,
		}
	
		return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")
			
	raise Http404

#upload slike
@csrf_exempt
def upload(request):

	if request.method == 'POST':
		
		raw_data = request.FILES['photo'].read()
		checksum = hashlib.md5()
		checksum.update(raw_data)
		checksum.hexdigest()

		image_type = imghdr.what('ignored', raw_data)
		if not image_type:
			to_json = {
				"success": 0,
				"error": "not an image"
			}
			return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")

		
		try:
			slika = Slika.objects.get(MD5 = checksum.hexdigest())
			to_json = {
				"success": 0,
			}
			return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")
		except Slika.DoesNotExist:
			
			photoHeight = request.POST['photoHeight']
			photoWidth = request.POST['photoWidth']
		
			slika = Slika(name = checksum.hexdigest() + '.' + image_type, MD5 = checksum.hexdigest(), latitude = None, longitude = None, height = photoHeight, width = photoWidth)
			slika.save()
			f = open(os.path.join(MEDIA_ROOT, str(slika.name) ), "wb+")
			f.write(raw_data)
			to_json = {
				"success": 1,
				"id": slika.pk
			}
			return HttpResponse(simplejson.dumps(to_json), mimetype="application/json")
		
	raise Http404
		