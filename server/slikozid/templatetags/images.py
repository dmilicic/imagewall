from django import template
register = template.Library()

@register.inclusion_tag('render_images.html')
def show_images(img_list):
    return {'img_list': img_list}