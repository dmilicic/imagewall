package com.slikozidKamera;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.*;

public class CameraActivity extends Activity{

    private Camera mCamera;
    private CameraPreview mPreview;
    private int numberOfCameras;
    public static String ipAddress = "team11.host25.com";
    public static int photoWidth = -1;
    public static int photoHeight = -1;
    public static File pictureFile;
    public static byte[] pictureData;

    public MyProgressDialog loadingDialog;
    public Context mainContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main);

        if(mCamera == null) initializeCamera(this);

        ImageButton captureButton = (ImageButton) findViewById(R.id.button_capture);
        LinearLayout captureLayout = (LinearLayout) findViewById(R.id.layout_capture);
        captureLayout.bringToFront();

        mainContext = this;
        loadingDialog = new MyProgressDialog(this);

        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        loadingDialog.show(mainContext, "", "");
                        mCamera.takePicture(null, null, mPicture);
                    }
                }
        );

    }

    /**
     * Trying to get camera instance
     * @return Instance of Camera or null if camera is unavailable
     */
    public Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
            numberOfCameras = Camera.getNumberOfCameras();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return c; // returns null if camera is unavailable
    }

    /**
     * Checks if device has camera on it
     * @param context Context for checking package manager feature Camera
     * @return true if device has camera, false otherwise
     */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Picture callback that writes data on the sd card in new thread
     *
     */
    public PictureCallback mPicture = new PictureCallback() {

        public void onPictureTaken(byte[] data, Camera camera) {

            pictureFile = getOutputMediaFile();
            pictureData = data;

            if (pictureFile == null){
                Log.d("SERVER", "Error creating media file, check storage permissions");
                return;
            }
            try {
                SavingThread t = new SavingThread();
                t.execute();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Thread that writes data to sd card and on finish call TagChoose Activity
     */
    public class SavingThread extends AsyncTask<Void, Void, Void> {

        @Override
        public Void doInBackground(Void... params){
            try{
                Log.d("SERVER", "POCINJE SEJVANJE");

                OutputStream fos = new FileOutputStream(pictureFile);
                fos.write(pictureData);
                fos.close();
                Log.d("SERVER", "ZAVRSI SEJVANJE");
            }
            catch(Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result){
            super.onPostExecute(result);

            Intent i;
            i = new Intent(getBaseContext(), TagChooser.class);

            //Toast.makeText(getApplicationContext(), "Picture saved to " + pictureFile.getAbsolutePath(), Toast.LENGTH_LONG).show();

            i.putExtra("ime", pictureFile.getAbsolutePath());
            i.putExtra("ipAddress", ipAddress);
            i.putExtra("photoWidth", photoWidth);
            i.putExtra("photoHeight", photoHeight);
            startActivity(i);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        releaseCamera();
    }

    @Override
    protected void onStop(){
        super.onStop();
        releaseCamera();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(mCamera == null)
            initializeCamera(this);
        loadingDialog.dismiss();
    }

    /**
     * Initializes camera instance and sets all its parameters
     * for it to be ready for use.
     * @param context Context for camera preview
     */
    private void initializeCamera(Context context){
        try{
            mCamera = getCameraInstance();

            Camera.Parameters parameters = mCamera.getParameters();

            Camera.Size pictureSize = getSmallestPictureSize(parameters);

            parameters.setPictureSize(pictureSize.width, pictureSize.height);
            parameters.setPictureFormat(ImageFormat.JPEG);
            mCamera.setParameters(parameters);

            mPreview = new CameraPreview(context, mCamera);
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mPreview);

            mPreview.bringToFront();
            mCamera.startPreview();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * releases camera and callback for other applications
     */
    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();
            mCamera = null;
        }
        mPreview.getHolder().removeCallback(mPreview);
    }

    /**
     * Makes a file on sdcard and returns it
     * @return File on sd card or null if sd card is not mounted
     */
    public static File getOutputMediaFile(){

        if(!Environment.getExternalStorageState().contentEquals(Environment.MEDIA_MOUNTED))
            return null;
        Log.d("SERVER", Environment.getExternalStorageState() + " " + Environment.MEDIA_MOUNTED);

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "slikozidKameraApp");

        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("slikozidKameraApp", "failed to create directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");

        return mediaFile;
    }

    /**
     * Returns smallest camera supported size for better performance
     * @param parameters Camera parameters
     * @return Camera smallest size
     */
    private Camera.Size getSmallestPictureSize(Camera.Parameters parameters) {
        Camera.Size result = null;

        for (Camera.Size size : parameters.getSupportedPictureSizes()) {
            if (result == null) {
                result=size;
            }
            else {
                int resultArea=result.width * result.height;
                int newArea=size.width * size.height;

                if (newArea < resultArea && newArea >= 1000000) {
                    result=size;
                    photoHeight = size.height;
                    photoWidth = size.width;
                }
            }
        }

        return(result);
    }

}