from django.db import models
from django_extensions.db.fields import AutoSlugField


# Create your models here.
class Hashtag(models.Model):
    tag = models.CharField(max_length=200)
    slug = AutoSlugField(populate_from="tag")
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.tag


class Slika(models.Model):
    name = models.CharField(max_length=200)
    MD5 = models.CharField(max_length=32)
    HashtagID = models.ForeignKey(Hashtag, null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    height = models.IntegerField()
    width = models.IntegerField()
	

    def __unicode__(self):
        return self.name
