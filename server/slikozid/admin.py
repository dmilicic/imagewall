from slikozid.models import Slika, Hashtag
from django.contrib import admin
 
admin.site.register(Slika)
admin.site.register(Hashtag)