from fabric.api import *

# Hosts to deploy onto
# env.hosts = ['www1.example.com', 'www2.example.com']

env.user = 'team11'
env.key_filename = 'mojkljuc'

# Where your project code lives on the server
env.project_root = '/home/team11/slikozid/server'

env.roledefs = {
  'prod': ['team11.host25.com']
}


@roles("prod")
def ls_on_prod():
    run("ls")


@roles("prod")
def deploy_prod(reset_db=False):
    with cd(env.project_root):
        run("git checkout master")
        # run("git reset --hard origin/master")
        run("git fetch origin")
        run("git merge --ff origin/master")
        if reset_db:
            run("python2.7 manage.py reset slikozid --noinput")
        run("python2.7 manage.py syncdb")
        run("python2.7 manage.py collectstatic -v0 --noinput")
        run("chmod 777 server/db")
        run("chmod 777 server/db/sqlite3.db")
        run("touch ~/public_html/django.wsgi")
