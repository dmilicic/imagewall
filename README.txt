ImageWall - Best Code Challenge 3.0
------------------------------------

Django web application built for the Best Code Challenge 3.0 competition.


Application consists of a web application and a mobile application. Web application displays all of the hashtags
that the users submitted and each tag represents a gallery of pictures associated with this tag. Mobile application
allows taking pictures and defining a tag for that picture after which it is uploaded to the server.

Django part of the application is located within server/ directory.
Android part of the application is located within slikozidKamera/ directory.

There is also documentation available in doc/ directory, however, documentation is only in Croatian and was required for the competition
evaluation committee.

## USAGE ##
-------

Running django application requires django installation: https://docs.djangoproject.com/en/dev/intro/install/
When django is installed, position yourself to the same directory as manage.py file.
Run from console: python manage.py runserver

Application should be made available at http://localhost:80