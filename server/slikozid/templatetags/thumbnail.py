import os
from wand.image import Image
from django import template
from django.conf import settings
from django.core.files import File

register = template.Library()

SCALE_WIDTH = 'w'
SCALE_HEIGHT = 'h'
SCALE_BOTH = 'both'


def scale(max_x, pair):
    x, y = pair
    new_y = (float(max_x) / x) * y
    return (int(max_x), int(new_y))


@register.filter
def thumbnail(file, size='200w'):
    # defining the size
    if (size.lower().endswith('h')):
        mode = 'h'
        size = size[:-1]
        max_size = int(size.strip())
    elif (size.lower().endswith('w')):
        mode = 'w'
        size = size[:-1]
        max_size = int(size.strip())
    else:
        mode = 'both'

    # f = open(os.path.join(settings.MEDIA_ROOT, str(file.pk) + '.jpg'), 'r')
    path = os.path.join(settings.MEDIA_ROOT, file.name)
    url = file.name

    # defining the filename and the miniature filename
    filehead, filetail = os.path.split(path)
    basename, format = os.path.splitext(filetail)
    miniature = basename + '_' + size + format
    filename = path
    miniature_filename = os.path.join(filehead, miniature)
    filehead, filetail = os.path.split(url)
    miniature_url = filehead + '/' + miniature
    if os.path.exists(miniature_filename) and os.path.getmtime(filename)>os.path.getmtime(miniature_filename):
        os.unlink(miniature_filename)

    # if the image wasn't already resized, resize it
    if not os.path.exists(miniature_filename):
        with Image(filename=filename) as image:
            image_x, image_y = image.size  
            
            if mode == SCALE_HEIGHT:
                image_y, image_x = scale(max_size, (image_y, image_x))
            elif mode == SCALE_WIDTH:
                image_x, image_y = scale(max_size, (image_x, image_y))
            elif mode == SCALE_BOTH:
                image_x, image_y = [int(x) for x in size.split('x')]
            else:
                raise Exception("Thumbnail size must be in ##w, ##h, or ##x## format.")
                
            image.resize(image_x, image_y)
            image.save(filename=miniature_filename)

    return miniature_url