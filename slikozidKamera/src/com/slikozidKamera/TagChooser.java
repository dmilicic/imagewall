package com.slikozidKamera;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.*;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: hrvoje
 * Date: 12/11/12
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class TagChooser extends Activity {

    private LocationManager locationManager;
    private LocationListener listenerCoarse;
    private LocationListener listenerFine;
    private Location currentLocation;
    private boolean locationAvailable = true;
    public static int photoID = -1;
    public static double latitude;
    public static double longitude;
    public static String ipAddress;
    public File pictureFile;
    public static int photoWidth = -1;
    public static int photoHeight = -1;
    public ListView listView;
    public Context mainContext;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.tag_chooser);
        registerLocationListeners();

        mainContext = this;

        Intent i = this.getIntent();
        String filePath = i.getStringExtra("ime");
        photoID = i.getIntExtra("photoID", -1);
        ipAddress = i.getStringExtra("ipAddress");
        photoWidth = i.getIntExtra("photoWidth", -1);
        photoHeight = i.getIntExtra("photoHeight", -1);

        refreshLatLong();

        pictureFile = new File(filePath);

        Resources res = getResources();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath);
        BitmapDrawable bd = new BitmapDrawable(res, bitmap);

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.tagLinear);
        linearLayout.setBackgroundDrawable(bd);

        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("photoWidth", Integer.toString(photoWidth));
        params.put("photoHeight", Integer.toString(photoHeight));
        try {
            params.put("photo", pictureFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        client.post("http://" + ipAddress + "/upload", params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                Log.d("SERVER", "POCEO CLIENT SLANJE");
            }


            @Override
            public void onFinish() {
                Log.d("SERVER", "ZAVRSIO CLIENT SLANJE");
            }

            @Override
            public void onSuccess(JSONObject object) {
                try {
                    if (object.getInt("success") == 1) photoID = object.getInt("id");
                    Toast.makeText(getBaseContext(), "Upload slike završen.", Toast.LENGTH_LONG).show();
                    Log.d("SERVER", "Dobio odgovor servera sa id slike= " + Integer.toString(photoID));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Throwable e, String response) {
                Log.d("SERVER", "PHOTO UPLOAD FAIL: " + response);
                stvoriAlertToast(0, "Greška pri komunikaciji sa serverom.");
                vratiSeNaKameru();
            }

        });




        listView = (ListView) findViewById(R.id.tagList);

        dohvatiTagove();

        TextView textView = (TextView) findViewById(R.id.tagText);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dohvatiTagove();
            }
        });


        TextView unosNovogTaga = (TextView) findViewById(R.id.noviTagText);
        unosNovogTaga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = unesiNoviTag();
                dialog.setCanceledOnTouchOutside(true);
                dialog.show();
            }
        });

    }

    /**
     * Creates alert dialog with title R.string.tagMessage_novi with Edit text
     * field of single line and positive button that sumbits the tag
     * @return AlertDialog that is ready to show for user
     */
    public AlertDialog unesiNoviTag() {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.tagMessage_novi);
        dialog.setInverseBackgroundForced(true);

        final View view = View.inflate(this, R.layout.unesi_novi_tag_dialog, null);
        final EditText naziv = (EditText) view.findViewById(R.id.unosTaga);
        dialog.setView(view);

        dialog.setPositiveButton(R.string.dodaj, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Log.d("GEOLOC", Double.toString(latitude) + " " + Double.toString(longitude));

                posaljiTagNaServer(naziv.getText().toString());

                dialogInterface.dismiss();


            }
        });

        return dialog.create();
    }

    /**
     * Sends POST request to server with 4 parameters: tag name, photo ID, latitude, longitude
     * In case photo is not uploaded it alerts that to user, otherwise on request success
     * it returns to CameraActivity with green happy smiley or red sad smiley depends on
     * server response
     * @param imeTaga name of the Tag type String
     */
    public void posaljiTagNaServer(String imeTaga){
        if(photoID == -1){
            Toast.makeText(getApplicationContext(), "Pricekajte upload slike...", Toast.LENGTH_SHORT).show();
            return;
        }
        RequestParams params = new RequestParams();
        params.put("tagName", imeTaga);
        params.put("id", Integer.toString(photoID));
        refreshLatLong();
        params.put("latitude", Double.toString(latitude));
        params.put("longitude", Double.toString(longitude));
        Log.d("SERVER", "GEO: slanje " + Double.toString(latitude) + " " + Double.toString(longitude));
        AsyncHttpClient client = new AsyncHttpClient();
        client.post("http://" + ipAddress + "/update", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject object) {
                try {
                    Log.d("SERVER", "SUCCESS upload: " + Integer.toString(object.getInt("success")));
                    stvoriAlertToast(1, "Upload je uspio.");
                    vratiSeNaKameru();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable e, String response) {
                Log.d("SERVER", "FAIL" + response);
                stvoriAlertToast(0, "Upload nije uspio.");
                vratiSeNaKameru();
            }
        });
    }

    /**
     * Starts new CameraActivity removing last CameraActivity and all
     * of it children from Activity stack
     */
    public void vratiSeNaKameru(){
        Intent intent = new Intent(getBaseContext(), CameraActivity.class);
        //i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    /**
     * Creates toast with LENGTH_LONG duration, message, and smiley face that can be
     * green if success is 1 or red if success is 0. Toast is ready to show.
     * @param success integer valued 1 if POST request was successful, 0 otherwise
     * @param message String of message to be displayed
     */
    public void stvoriAlertToast(int success, String message){
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.alert_toast,
                (ViewGroup) findViewById(R.id.toastViewGroup));

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        ImageView toastImage = (ImageView) view.findViewById(R.id.toastImage);
        TextView toastText = (TextView) view.findViewById(R.id.toastText);

        if(success == 1)
            toastImage.setImageResource(R.drawable.smiley_mood_happy_green);

        if(success == 0)
            toastImage.setImageResource(R.drawable.smiley_mood_sad_red);

        toastText.setText(message);
        toast.setView(view);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();

    }

    /**
     * refreshes latitude and longitude and send them as parameters of POST request to server
     * and gets array of closest tags. Fills ArrayList with returned tags.
     */
    public void dohvatiTagove(){
        RequestParams params = new RequestParams();
        refreshLatLong();
        params.put("latitude", Double.toString(latitude));
        params.put("longitude", Double.toString(longitude));
        Log.d("SERVER", "GEO: slanje " + Double.toString(latitude) + " " + Double.toString(longitude));

        AsyncHttpClient client = new AsyncHttpClient();
        client.post("http://" + ipAddress + "/tag-request", params, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                Log.d("SERVER", "POCEO DOHVACANJE TAGOVA");
            }


            @Override
            public void onFinish() {
                Log.d("SERVER", "ZAVRSIO DOHVACANJE TAGOVA");
            }
            @Override
            public void onSuccess(JSONObject object) {
                try {

                    JSONArray array = (JSONArray) object.get("tags");
                    String[] result = new String[array.length()];
                    Log.d("SERVER", "DOHVATIO TAGOVE: "+ Double.toString(latitude) + " " + Double.toString(longitude) + "\n" + object.toString());
                    for(int i = 0; i < array.length(); i++){
                        result[i] = (String) array.get(i);
                    }

                    TextView textView = (TextView) findViewById(R.id.tagText);

                    if(array.length() == 0) textView.setText("Nema bližih tagova");
                    else textView.setText("Izaberi tag slike");

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(mainContext, R.layout.list_item, result);
                    listView.setAdapter(adapter);

                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView parent, View v, int position, long id){
                            posaljiTagNaServer(((TextView)v.findViewById(R.id.list_item_text)).getText().toString());
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable e, String response) {
                Log.d("SERVER", "FAIL DOHVACANJA TAGOVA" + response);
            }
        });
    }


    /**
     * refreshes latitude and longitude geo location parameters
     */
    public void refreshLatLong(){
        if(currentLocation != null){
            latitude = currentLocation.getLatitude();
            longitude = currentLocation.getLongitude();
        }
    }

    /**
     * registers geo location listeners
     */
    private void registerLocationListeners() {
        locationManager = (LocationManager)
                getSystemService(LOCATION_SERVICE);

        // Initialize criteria for location providers
        Criteria fine = new Criteria();
        fine.setAccuracy(Criteria.ACCURACY_FINE);
        Criteria coarse = new Criteria();
        coarse.setAccuracy(Criteria.ACCURACY_COARSE);

        // Get at least something from the device,
        // could be very inaccurate though
        currentLocation = locationManager.getLastKnownLocation(
                locationManager.getBestProvider(fine, true));

        if (listenerFine == null || listenerCoarse == null)
            createLocationListeners();

        // Will keep updating about every 500 ms until
        // accuracy is about 1000 meters to get quick fix.
        locationManager.requestLocationUpdates(
                locationManager.getBestProvider(coarse, true),
                500, 1000, listenerCoarse);
        // Will keep updating about every 500 ms until
        // accuracy is about 50 meters to get accurate fix.
        locationManager.requestLocationUpdates(
                locationManager.getBestProvider(fine, true),
                500, 50, listenerFine);
    }

    /**
     * creates geo location listeners
     */
    private void createLocationListeners() {
        listenerCoarse = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
                switch(status) {
                    case LocationProvider.OUT_OF_SERVICE:
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        locationAvailable = false;
                        break;
                    case LocationProvider.AVAILABLE:
                        locationAvailable = true;
                }
            }
            public void onProviderEnabled(String provider) {
            }
            public void onProviderDisabled(String provider) {
            }
            public void onLocationChanged(Location location) {
                currentLocation = location;
                if (location.getAccuracy() > 1000 && location.hasAccuracy())
                    locationManager.removeUpdates(this);
            }
        };

        listenerFine = new LocationListener() {
            public void onStatusChanged(String provider,
                                        int status, Bundle extras) {
                switch(status) {
                    case LocationProvider.OUT_OF_SERVICE:
                    case LocationProvider.TEMPORARILY_UNAVAILABLE:
                        locationAvailable = false;
                        break;
                    case LocationProvider.AVAILABLE:
                        locationAvailable = true;
                }
            }
            public void onProviderEnabled(String provider) {
            }
            public void onProviderDisabled(String provider) {
            }
            public void onLocationChanged(Location location) {
                currentLocation = location;
                if (location.getAccuracy() > 1000 && location.hasAccuracy())
                locationManager.removeUpdates(this);
            }
        };
    }

    @Override
    protected void onResume() {
        registerLocationListeners();
        super.onResume();
    }

    @Override
    protected void onPause() {
        locationManager.removeUpdates(listenerCoarse);
        locationManager.removeUpdates(listenerFine);
        super.onPause();
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        vratiSeNaKameru();
    }

}