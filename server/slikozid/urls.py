from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView
from django.views.static import * 
from django.db.models import Count, Max
from django.conf import settings
from slikozid import views
from slikozid.models import Hashtag, Slika
from django.conf.urls.static import static

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'tag-request', views.tag_request, name = 'tag-request'),
	url(r'upload' , views.upload, name = 'upload'),
	url(r'update' , views.update, name = 'update'),
	
	url(r'galerija/{1}$', ListView.as_view(
					queryset = Slika.objects.all(),
					context_object_name = 'img_list',
					template_name = 'slikozid/gallery.html',
				), name='gallery'),
				
	url(r'galerija/(?P<hashtag>.*)$', views.hash, name = "hash"),
  url(r'embed/(?P<hashtag>.*)$', views.embed, name = "embed"),  
	url(r'slika/(?P<image_id>\d+)$', views.image, name = "image"),
	url(r'autocomplete', views.navigation_autocomplete, name='navigation_autocomplete'),  
) 

#Server debug
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )
    urlpatterns += patterns('django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
